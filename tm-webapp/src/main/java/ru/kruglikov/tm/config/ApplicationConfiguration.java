package ru.kruglikov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.kruglikov.tm")
public class ApplicationConfiguration {
}

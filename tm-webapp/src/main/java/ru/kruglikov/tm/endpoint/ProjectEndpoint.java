package ru.kruglikov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kruglikov.tm.api.endpoint.IProjectRestEndpoint;
import ru.kruglikov.tm.model.Project;
import ru.kruglikov.tm.service.ProjectService;

import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @GetMapping("/findAll")
    public @Nullable Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @GetMapping("/findById/{id}")
    public @Nullable Project findById(@PathVariable("id") final String id) {
        return projectService.findOneById(id);
    }

    @Override
    @PostMapping("/save")
    public @NotNull Project save(@RequestBody final Project project) {
        projectService.add(project);
        return project;
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody final Project project) {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void delete(@PathVariable("id") final String id) {
        projectService.removeById(id);
    }

}
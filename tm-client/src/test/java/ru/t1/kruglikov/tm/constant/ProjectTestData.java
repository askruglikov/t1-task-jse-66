package ru.t1.kruglikov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static String PROJECT1_NAME = "PROJECT_01";

    @NotNull
    public final static String PROJECT1_DESC = "DESCRIPTION_01";

    @NotNull
    public final static String PROJECT2_NAME = "PROJECT_02";

    @NotNull
    public final static String PROJECT2_DESC = "DESCRIPTION_02";

    @NotNull
    public final static String PROJECT3_NAME = "PROJECT_03";

    @NotNull
    public final static String PROJECT3_DESC = "DESCRIPTION_03";

}

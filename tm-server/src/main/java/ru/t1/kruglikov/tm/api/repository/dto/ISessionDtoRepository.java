package ru.t1.kruglikov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.kruglikov.tm.dto.model.SessionDTO;
import ru.t1.kruglikov.tm.enumerated.SessionSort;

import java.util.List;

@Repository
@Scope("prototype")
public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDTO> {

    @Nullable
    @Query("SELECT p FROM SessionDTO p ORDER BY :sort")
    List<SessionDTO> findAll(@NotNull String sort);

}
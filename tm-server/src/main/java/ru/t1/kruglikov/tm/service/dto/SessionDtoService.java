package ru.t1.kruglikov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.kruglikov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.kruglikov.tm.api.repository.model.IUserRepository;
import ru.t1.kruglikov.tm.api.service.IConnectionService;
import ru.t1.kruglikov.tm.api.service.dto.ISessionDtoService;
import ru.t1.kruglikov.tm.dto.model.SessionDTO;
import ru.t1.kruglikov.tm.enumerated.SessionSort;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionDtoRepository> implements ISessionDtoService {

    @NotNull
    @Autowired
    private ISessionDtoRepository repository;

    @Nullable
    @Override
    public List<SessionDTO> findAll(@Nullable SessionSort sort){
        if (sort == null) return repository.findAll();
        return repository.findAll(sort.getColumnName());
    };

}
